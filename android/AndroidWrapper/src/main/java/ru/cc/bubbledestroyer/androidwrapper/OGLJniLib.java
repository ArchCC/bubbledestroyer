package ru.cc.bubbledestroyer.androidwrapper;

import java.nio.ByteBuffer;

/**
 * Created by aleksandrsemenov on 09.08.14.
 */
public class OGLJniLib {
    static {
        System.loadLibrary("bubblejni");
    }

    /**
     * @param width the current view width
     * @param height the current view height
     */
    //public static native void init(int width, int height);
    //public static native void step();
    public static long nativePointer = 0;

    public static native long createGameEngine(float width, float height);
    public static native void setupTexture(long nativePointer, int objType, int frNumber, ByteBuffer buf, int wid, int hei);
    public static native void updateAnimation(long nativePointer);
    public static native void showMainScreen(long nativePointer);
    public static native void startNewDeadlyLine(long nativePointer,  float x, float y);
    public static native void addPointToDeadlyLine(long nativePointer, float x, float y);
    public static native void testLine(long nativePointer);
}
