package ru.cc.bubbledestroyer.androidwrapper;

import android.app.Activity;
import android.opengl.GLES20;
import android.opengl.GLSurfaceView;
import android.os.Bundle;

public class OGLActivity extends Activity {

    private OGLView oglView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        oglView = new OGLView(getApplication());
        oglView.setRenderMode(GLSurfaceView.RENDERMODE_CONTINUOUSLY);
        setContentView(oglView);
    }

    @Override protected void onPause() {
        super.onPause();
        oglView.onPause();
    }

    @Override protected void onResume() {
        super.onResume();
        oglView.onResume();
    }
}
