//
//  BDCOpenGlView.h
//  BubbleDestroyer
//
//  Created by Aleksandr Semenov on 15.07.14.
//  Copyright (c) 2014 Aleksandr Semenov. All rights reserved.
//

#ifndef __BubbleDestroyer__OpenGlView__
#define __BubbleDestroyer__OpenGlView__

#import <UIKit/UIKit.h>
#import <QuartzCore/QuartzCore.h>
#include <OpenGLES/ES2/gl.h>
#include <OpenGLES/ES2/glext.h>
#include "GameEngine.h"
#include "Logger.h"

@interface BDCOpenGlView : UIView {
    CAEAGLLayer* _eaglLayer;
    EAGLContext* _context;
    int fps;
    
    GLuint m_framebuffer;
    GLuint m_renderbuffer;
    
    GameEngine* gameEngine;
}

-(GameEngine*) getGameEngine;

@end

#endif /* defined(__BubbleDestroyer__OpenGlView__) */