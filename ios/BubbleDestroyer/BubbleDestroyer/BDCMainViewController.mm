//
//  BDCMainViewController.m
//  BubbleDestroyer
//
//  Created by Aleksandr Semenov on 15.07.14.
//  Copyright (c) 2014 Aleksandr Semenov. All rights reserved.
//

#import "BDCMainViewController.h"

@interface BDCMainViewController ()

@end

@implementation BDCMainViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
	
}

- (void)viewDidAppear:(BOOL)animated {
    //Создание вьюхи с openGl
    openGlView = [[BDCOpenGlView alloc] initWithFrame:self.view.bounds];
    [self.view addSubview:openGlView];
    
    shapeLayer = [CAShapeLayer layer];
    shapeLayer.strokeColor = [[UIColor blueColor] CGColor];
    shapeLayer.lineWidth = 3.0;
    shapeLayer.fillColor = [[UIColor clearColor] CGColor];
}

//Про тачи я могу сказать только одно, если прислонить много пальцев то будет весело ^_^ В общем баг, но решить не успел
- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    [shapeLayer removeFromSuperlayer];
    
    [touches enumerateObjectsUsingBlock:^(id obj, BOOL *stop) {
        // Get a single touch and it's location
        UITouch *touch = obj;
        CGPoint touchPoint = [touch locationInView:[self view]];
        //При первом касании начинаем новую линию
        openGlView.getGameEngine->startNewDeadlyLine(touchPoint.x, touchPoint.y);
        
        NSLog(@"began: %@", NSStringFromCGPoint(touchPoint));
        
        //В ТЗ ничего не говорилось про отрисовывание линии, по которой расчитывается разрезание, так что можно было бы и убрать, но оставлю для более наглядного теста(перенести в openGl не успел)
        path = [UIBezierPath bezierPath];
        
        [path moveToPoint:CGPointMake(touchPoint.x, touchPoint.y)];
        
    }];
}

-(void)touchesMoved:(NSSet *)touches withEvent:(UIEvent *)event {
    [touches enumerateObjectsUsingBlock:^(id obj, BOOL *stop) {
        // Get a single touch and it's location
        UITouch *touch = obj;
        CGPoint touchPoint = [touch locationInView:[self view]];
        
        //Добавляем очередную точку в линию
        openGlView.getGameEngine->addPointToDeadlyLine(touchPoint.x, touchPoint.y);
        
        
        //Опять таки оставлено для теста
        [path addLineToPoint:CGPointMake(touchPoint.x, touchPoint.y)];
        [shapeLayer removeFromSuperlayer];
        shapeLayer.path = [path CGPath];
        
        [self.view.layer addSublayer:shapeLayer];
        NSLog(@"movied: %@", NSStringFromCGPoint(touchPoint));
    }];
}

-(void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event {
    /*if (!openGlView.getGameEngine->IsGameStarted()) {
        openGlView.getGameEngine->StartGame();
        return;
    }*/
    
    [touches enumerateObjectsUsingBlock:^(id obj, BOOL *stop) {
        // Get a single touch and it's location
        UITouch *touch = obj;
        CGPoint touchPoint = [touch locationInView:[self view]];
        NSLog(@"ended2: %@", NSStringFromCGPoint(touchPoint));
        
        //тест
        [path addLineToPoint:CGPointMake(touchPoint.x, touchPoint.y)];
        
        //ставим последнюю точку в линии и начинаем проверку
        openGlView.getGameEngine->addPointToDeadlyLine(touchPoint.x, touchPoint.y);
        openGlView.getGameEngine->testLine();
    }];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
