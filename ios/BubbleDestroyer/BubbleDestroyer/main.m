//
//  main.m
//  BubbleDestroyer
//
//  Created by Aleksandr Semenov on 06.08.14.
//  Copyright (c) 2014 Aleksandr Semenov. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "BDCAppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([BDCAppDelegate class]));
    }
}
