//
//  BDCMainViewController.h
//  BubbleDestroyer
//
//  Created by Aleksandr Semenov on 15.07.14.
//  Copyright (c) 2014 Aleksandr Semenov. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BDCOpenGlView.h"

@interface BDCMainViewController : UIViewController {

    UIBezierPath* path;
    CAShapeLayer *shapeLayer;
    BDCOpenGlView* openGlView;
}

@end
