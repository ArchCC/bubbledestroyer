//
//  BDCOpenGlView.m
//  BubbleDestroyer
//
//  Created by Aleksandr Semenov on 15.07.14.
//  Copyright (c) 2014 Aleksandr Semenov. All rights reserved.
//

#import "BDCOpenGlView.h"

@implementation BDCOpenGlView

// Логирование производительности
-(void)timerFired:(NSTimer*) aTimer {
    NSLog([@"FPS: " stringByAppendingString:[[NSNumber numberWithInt:fps] stringValue]]);
    fps = 0;
}

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    [self setMultipleTouchEnabled:YES];
    
    //Логирование производительности
    NSTimer *timer = [NSTimer timerWithTimeInterval:1.0 target:self selector:@selector(timerFired:) userInfo:nil repeats:YES];
    [[NSRunLoop mainRunLoop] addTimer:timer forMode:NSDefaultRunLoopMode];
    
    //openGl инициализация
    if (self) {
        [self setupLayer];
        [self setupContext];
        [self setupRenderEngineWithFrame:frame];
    }
    return self;
}

+ (Class)layerClass {
    return [CAEAGLLayer class];
}

- (void)setupLayer {
    _eaglLayer = (CAEAGLLayer*) self.layer;
    _eaglLayer.opaque = YES;
}

- (void)setupContext {
    EAGLRenderingAPI api = kEAGLRenderingAPIOpenGLES2;
    _context = [[EAGLContext alloc] initWithAPI:api];
    if (!_context) {
        NSLog(@"Failed to initialize OpenGLES 2.0 context");
        exit(1);
    }
    
    if (![EAGLContext setCurrentContext:_context]) {
        NSLog(@"Failed to set current OpenGL context");
        exit(1);
    }
}

- (void)setupRenderEngineWithFrame:(CGRect)frame {
    //Игровой движек
    gameEngine = new GameEngine();
    
    glGenRenderbuffers(1, &m_renderbuffer);
    glBindRenderbuffer(GL_RENDERBUFFER, m_renderbuffer);
    
    [_context renderbufferStorage:GL_RENDERBUFFER fromDrawable: _eaglLayer];
    
    glGenFramebuffers(1, &m_framebuffer);
    glBindFramebuffer(GL_FRAMEBUFFER, m_framebuffer);
    glFramebufferRenderbuffer(GL_FRAMEBUFFER,
                              GL_COLOR_ATTACHMENT0,
                              GL_RENDERBUFFER,
                              m_renderbuffer);
    
    GLenum status = glCheckFramebufferStatus(GL_FRAMEBUFFER);
    if(status != GL_FRAMEBUFFER_COMPLETE) {
        LOGI("frame buffer error with error: %d",status);
    }
    
    //Инициализация... ну и так понятно
    gameEngine->Initialize(CGRectGetWidth(frame), CGRectGetHeight(frame));
    
    //Текстуры для шариков и "меню"
    [self loadTextureForObjectType:0 withFrameNumber:0 fromFile:@"bright_bubble" withExtension:@"png"];
    [self loadTextureForObjectType:1 withFrameNumber:0 fromFile:@"dark_bubble" withExtension:@"png"];
    [self loadTextureForObjectType:2 withFrameNumber:0 fromFile:@"main_screen" withExtension:@"png"];
    
    [self drawView: nil];
    
    CADisplayLink* displayLink;
    displayLink = [CADisplayLink displayLinkWithTarget:self selector:@selector(drawView:)];
    [displayLink addToRunLoop:[NSRunLoop currentRunLoop] forMode:NSDefaultRunLoopMode];
    
    //Отображаем "меню"
    gameEngine->ShowMainScreen();
    [_context presentRenderbuffer:GL_RENDERBUFFER];
    
}

//Загрузка текстуры
- (void) loadTextureForObjectType: (int) objectType withFrameNumber:(int) frameNumber fromFile:(NSString*) fileName withExtension:(NSString*) fileExtension
{
    NSString *path = [[NSBundle mainBundle] pathForResource:fileName ofType:fileExtension];
    NSData *texData = [[NSData alloc] initWithContentsOfFile:path];
    UIImage *image = [[UIImage alloc] initWithData:texData];
    if (image == nil)
        NSLog(@"Do real error checking here");
    
    GLuint width = CGImageGetWidth(image.CGImage);
    GLuint height = CGImageGetHeight(image.CGImage);
    CGColorSpaceRef colorSpace = CGColorSpaceCreateDeviceRGB();
    void *imageData = malloc( height * width * 4 );
    CGContextRef context = CGBitmapContextCreate( imageData, width, height, 8, 4 * width, colorSpace, kCGImageAlphaPremultipliedLast | kCGBitmapByteOrder32Big );
    CGColorSpaceRelease( colorSpace );
    CGContextClearRect( context, CGRectMake( 0, 0, width, height ) );
    CGContextTranslateCTM( context, 0, height - height );
    CGContextDrawImage( context, CGRectMake( 0, 0, width, height ), image.CGImage );
    
    /*if (width < 10) {
        NSData *data = (NSData *)CFBridgingRelease(CGDataProviderCopyData(CGImageGetDataProvider(image.CGImage)));
        const char *bytes = (const char *)[data bytes];
        int len = [data length];
        
        // Note: this assumes 32bit RGBA
        for (int i = 0; i < len; i += 4) {
            char r = bytes[i];
            
            for(int i = 0; i < 8; i ++) {
                //bits = [NSString stringWithFormat:@"%i%@", value & (1 << i) ? 1 : 0, bits];
                LOGI("%i",r & (1 << i) ? 1 : 0);
            }
            
            char g = bytes[i+1];
            for(int i = 0; i < 8; i ++) {
                //bits = [NSString stringWithFormat:@"%i%@", value & (1 << i) ? 1 : 0, bits];
                LOGI("%i",g & (1 << i) ? 1 : 0);
            }
            char b = bytes[i+2];
            for(int i = 0; i < 8; i ++) {
                //bits = [NSString stringWithFormat:@"%i%@", value & (1 << i) ? 1 : 0, bits];
                LOGI("%i",b & (1 << i) ? 1 : 0);
            }
            char a = bytes[i+3];
            for(int i = 0; i < 8; i ++) {
                //bits = [NSString stringWithFormat:@"%i%@", value & (1 << i) ? 1 : 0, bits];
                LOGI("%i",a & (1 << i) ? 1 : 0);
            }
        }
    }*/
    
    gameEngine->GetGraphicEngine()->SetupTexture(objectType, frameNumber, imageData, width, height);
    
    CGContextRelease(context);
    
    free(imageData);
}

- (void) drawView: (CADisplayLink*) displayLink
{
    fps++;
    
    //Вызов игровых расчетов
    gameEngine->UpdateAnimation();
    [_context presentRenderbuffer:GL_RENDERBUFFER];
}

-(GameEngine*) getGameEngine {
    return gameEngine;
}

@end
