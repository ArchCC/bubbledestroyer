//
//  SceneObject.h
//  BubbleDestroyer
//
//  Сущность игрового объекта
//
//  Created by Aleksandr Semenov on 16.07.14.
//  Copyright (c) 2014 Aleksandr Semenov. All rights reserved.
//

//Вот такое вот представление объекта для отрисовки

#ifndef __BubbleDestroyer__SceneObject__
#define __BubbleDestroyer__SceneObject__

#include <iostream>
#include <string>
#include "GraphicObject.h"
#include <chrono>
#include <future>
#include <cmath>

class SceneObject {
public:
    //Конструктор с типом
    SceneObject(int);
    
    int objectType;
    
    //Вершины для отрисовки
    GraphicObject::Vertex *verteces;
    
    //Характеристики
    float rotation[2];
    float position[2];
    float scale;
    float speed[2];
    float speedAngle[2];
    float birthTime;

    bool removed;
    
    //Функции для манипуляции в своей очереди
    void setNextSceneObject(SceneObject*);
    SceneObject* getNextSceneObject();
    void setPrevSceneObject(SceneObject*);
    SceneObject* getPrevSceneObject();
    void setParentObject(SceneObject*);
    SceneObject* getParentObject();
    
protected:
    
    SceneObject* nextSceneObject = nullptr;
    SceneObject* prevSceneObject = nullptr;
    
    SceneObject* parentObject = nullptr;
    
    typedef std::chrono::high_resolution_clock Time;
};

#endif /* defined(__BubbleDestroyer__SceneObject__) */
