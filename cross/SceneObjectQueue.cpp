//
//  SceneObjectQueue.cpp
//  BubbleDestroyer
//
//  Created by Aleksandr Semenov on 16.07.14.
//  Copyright (c) 2014 Aleksandr Semenov. All rights reserved.
//

//Вот такая вот коллекция для хранения и более простых манипуляций с очередями для отрисовки, велосипед, но оказалось быстрее чем найти подходящую замену

#include "SceneObjectQueue.h"

//Добавление в очередь
void SceneObjectQueue::addSceneObject(SceneObject* sceneObject)
{
    addSceneObjectAfter(sceneObject, lastSceneObject);
}

//Удаление из очереди
void SceneObjectQueue::removeSceneObject(SceneObject* sceneObject)
{
    if (sceneObject->getPrevSceneObject() != nullptr) {
        sceneObject->getPrevSceneObject()->setNextSceneObject(sceneObject->getNextSceneObject());
    } else {
        firstSceneObject = sceneObject->getNextSceneObject();
    }
    if (sceneObject->getNextSceneObject() != nullptr) {
        sceneObject->getNextSceneObject()->setPrevSceneObject(sceneObject->getPrevSceneObject());
    } else {
        lastSceneObject = sceneObject->getPrevSceneObject();
    }
}

//Замена на другой объект
void SceneObjectQueue::replaceWithObject(SceneObject* sceneObject, SceneObject* withObject)
{
    if (sceneObject->getPrevSceneObject() != nullptr) {
        withObject->setPrevSceneObject(sceneObject->getPrevSceneObject());
        sceneObject->getPrevSceneObject()->setNextSceneObject(withObject);
    } else {
        firstSceneObject = withObject;
    }
    if (sceneObject->getNextSceneObject() != nullptr) {
        withObject->setNextSceneObject(sceneObject->getNextSceneObject());
        sceneObject->getNextSceneObject()->setPrevSceneObject(withObject);
    } else {
        lastSceneObject = withObject;
    }
}

//Очистка... нужно переписать для человеческой работы с памятью
void SceneObjectQueue::clear()
{
    SceneObject* curObj = firstSceneObject;
    for (;;) {
        if (curObj == nullptr) {
            break;
        }
        
        SceneObject* objToRemove = curObj;
        curObj = curObj->getNextSceneObject();
        delete objToRemove;
    }
    
    firstSceneObject = nullptr;
    lastSceneObject = nullptr;
}

//Более глубокий уровень добавления =)
void SceneObjectQueue::addSceneObjectAfter(SceneObject* object, SceneObject* afterObject)
{
    if (firstSceneObject == nullptr) {
        firstSceneObject = object;
        lastSceneObject = object;
        return;
    }
    object->setPrevSceneObject(afterObject);
    object->getPrevSceneObject()->setNextSceneObject(object);
    object->setNextSceneObject(nullptr);
    lastSceneObject = object;
}

//Получение головы
SceneObject* SceneObjectQueue::getFirstSceneObject()
{
    return firstSceneObject;
}