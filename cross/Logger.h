//
//  Logger.h
//  Bubble
//
//  Created by Aleksandr Semenov on 09.08.14.
//  Copyright (c) 2014 Aleksandr Semenov. All rights reserved.
//

#ifndef Bubble_Logger_h
#define Bubble_Logger_h

#ifdef __APPLE__
    #define  LOGI(...)  printf(__VA_ARGS__)
#elif __ANDROID__
    #include <android/log.h>
    #define  LOG_TAG    "libbubblejni"
    #define  LOGI(...)  __android_log_print(ANDROID_LOG_INFO,LOG_TAG,__VA_ARGS__)
    #define  LOGE(...)  __android_log_print(ANDROID_LOG_ERROR,LOG_TAG,__VA_ARGS__)
#endif

#endif
