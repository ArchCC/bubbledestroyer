//
//  BDCLine.h
//  BubbleDestroyer
//
//  Created by Aleksandr Semenov on 19.07.14.
//  Copyright (c) 2014 Aleksandr Semenov. All rights reserved.
//

//Вот такая вот ерунда используется для хранения точек линии, которой разрезаются шары

#ifndef __BubbleDestroyer__BDCLine__
#define __BubbleDestroyer__BDCLine__

#include <iostream>
#include <vector>

struct BDCPoint {
    float x;
    float y;
};

class BDCLine {
public:
    BDCLine() {
        
    }
    
    void addPoint(float, float);
    std::vector<BDCPoint> getPoints();
    void clear();
private:
    std::vector<BDCPoint> points;
};

#endif /* defined(__BubbleDestroyer__BDCLine__) */
