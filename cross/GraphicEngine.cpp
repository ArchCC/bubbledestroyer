//
//  GraphicEngine.cpp
//  BubbleDestroyer
//
//  Created by Aleksandr Semenov on 15.07.14.
//  Copyright (c) 2014 Aleksandr Semenov. All rights reserved.
//

#include "GraphicEngine.h"

//=============================================================================================
//================                      INITIALIZATION                         ================
//=============================================================================================
//Работа с openGl взята из моего более другого тестового задания ^_^
//=============================================================================================
static void checkGlError(const char* op) {
    for (GLint error = glGetError(); error; error = glGetError()) {
        LOGI("after %s() glError (0x%x)\n", op, error);
    }
}

GraphicEngine::GraphicEngine() {
}

void GraphicEngine::Initialize(int width, int height, int xAspect, int yAspect)
{
    LOGI("Init graphic engine\n");
    glViewport(0, 0, width, height);
    //Texturing enable {
    glEnable(GL_TEXTURE_2D);                            //error 1280
    glEnable(GL_BLEND);
    glBlendFunc(GL_ONE, GL_ONE_MINUS_SRC_ALPHA);
    //glBlendFunc(GL_SRC_ALPHA, GL_ONE);
    
    checkGlError("enableTexturing");
    
    m_simpleProgram = BuildProgram(SimpleVertexShader, SimpleFragmentShader);
    
    glUseProgram(m_simpleProgram);
    
    // Initialize the projection matrix.
    ApplyOrtho(xAspect, yAspect);
}

void GraphicEngine::ApplyOrtho(float maxX, float maxY) const
{
    float a = 1.0f / maxX;
    float b = 1.0f / maxY;
    float ortho[16] = {
        a, 0,  0, 0,
        0, b,  0, 0,
        0, 0, -1, 0,
        0, 0,  0, 1
    };
    
    GLint projectionUniform = glGetUniformLocation(m_simpleProgram, "Projection");
    glUniformMatrix4fv(projectionUniform, 1, 0, &ortho[0]);
}

void GraphicEngine::SetupTexture(int type, int frame, void *imgData, GLuint width, GLuint height)
{
    /*LOGI("w:%d,h:%d\n",width,height);
    if (width < 10) {
        for (int i = 0; i < width * height; i++) {
            char ch = *(((char*)imgData)+i);
            LOGI("%d",ch);
            //std::bitset<8> x(ch);
            //std::cout << x;
        }
    }*/
    GLuint tmpIndex;
    glGenTextures(1, &tmpIndex);
    glActiveTexture(GL_TEXTURE0);
    glBindTexture(GL_TEXTURE_2D, tmpIndex);
    
    textures.insert(std::pair<int, GLuint>(type, tmpIndex));
    
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
    
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, width, height, 0, GL_RGBA, GL_UNSIGNED_BYTE, imgData);
    
    checkGlError("setup texture");
    LOGI("\ntextere setted with id %d\n",tmpIndex);
}


GLuint GraphicEngine::BuildShader(const char* source, GLenum shaderType) const
{
    GLuint shaderHandle = glCreateShader(shaderType);
    glShaderSource(shaderHandle, 1, &source, 0);
    glCompileShader(shaderHandle);
    
    GLint compileSuccess;
    glGetShaderiv(shaderHandle, GL_COMPILE_STATUS, &compileSuccess);
    
    if (compileSuccess == GL_FALSE) {
        GLchar messages[256];
        glGetShaderInfoLog(shaderHandle, sizeof(messages), 0, &messages[0]);
        std::cout << messages;
        exit(1);
    }
    
    return shaderHandle;
}

GLuint GraphicEngine::BuildProgram(const char* vertexShaderSource,
                                   const char* fragmentShaderSource) const
{
    GLuint vertexShader = BuildShader(vertexShaderSource, GL_VERTEX_SHADER);
    GLuint fragmentShader = BuildShader(fragmentShaderSource, GL_FRAGMENT_SHADER);
    
    GLuint programHandle = glCreateProgram();
    glAttachShader(programHandle, vertexShader);
    glAttachShader(programHandle, fragmentShader);
    glLinkProgram(programHandle);
    
    GLint linkSuccess;
    glGetProgramiv(programHandle, GL_LINK_STATUS, &linkSuccess);
    if (linkSuccess == GL_FALSE) {
        GLchar messages[256];
        glGetProgramInfoLog(programHandle, sizeof(messages), 0, &messages[0]);
        std::cout << messages;
        exit(1);
    }
    
    return programHandle;
}

//=============================================================================================
//================                       RENDERING                             ================
//=============================================================================================

//Основная отрисовка, все матричные расчеты вынесены в шейдер, отсюда установка тонны переменных, которые привели меня практически к границам допустимого, но, честно говоря, я даже и не знаю как это делается правильно, в книго-интернетах все делают так как хотят, в целом я склоняюсь к расчету всего и вся на cpu, просто не перенес в собственные библиотеки, а использовать стандартные не позволяют тестовые задания в более других конторах ^_^

//Ах да, все объекты рисуются через текстурки, выглядит дешего и сердито, но может я и не прав =)
void GraphicEngine::RenderScene(SceneObjectQueue* sceneObjects)
{
    glClearColor(0, 0.3f, 0, 1.0);
    glClear(GL_COLOR_BUFFER_BIT);
    
    GLuint colorSlot = glGetAttribLocation(m_simpleProgram, "SourceColor");
    GLuint positionSlot = glGetAttribLocation(m_simpleProgram, "Position");
    GLuint translationSlot = glGetAttribLocation(m_simpleProgram, "Translation");
    GLuint rotationSlot = glGetAttribLocation(m_simpleProgram, "Rotation");
    GLuint scaleSlot = glGetAttribLocation(m_simpleProgram, "Scale");
    GLuint texCoordSlot = glGetAttribLocation(m_simpleProgram, "TexCoordIn");
    GLuint textureUniform = glGetUniformLocation(m_simpleProgram, "Texture");
    
    glEnableVertexAttribArray(positionSlot);
    glEnableVertexAttribArray(colorSlot);
    glEnableVertexAttribArray(texCoordSlot);
    
    GLsizei stride = sizeof(GraphicObject::Vertex);
    
    SceneObject* curObj = sceneObjects->getFirstSceneObject();
    for (;;) {
        if (curObj == nullptr) {
            break;
        }
        
        if (curObj->removed) {
            curObj = curObj->getNextSceneObject();
            continue;
        }
        
        //LOGI("obj type: %d, obj position: %f",curObj->objectType, curObj->position[1]);
        
        const GLvoid* pColors = &GraphicObject::getVertecesForGameObject()->Color[0];
        const GLvoid* pCoords = &GraphicObject::getVertecesForGameObject()->Position[0];
        const GLvoid* pTexCoords = &GraphicObject::getVertecesForGameObject()->TexCoord[0];
        
        glVertexAttribPointer(colorSlot, 4, GL_FLOAT, GL_FALSE, stride, pColors);
        glVertexAttribPointer(positionSlot, 2, GL_FLOAT, GL_FALSE, stride, pCoords);
        glVertexAttrib2f(translationSlot,curObj->position[0],curObj->position[1]);
        glVertexAttrib2f(rotationSlot,curObj->rotation[0],curObj->rotation[1]);
        glVertexAttrib1f(scaleSlot,curObj->scale);
        glVertexAttribPointer(texCoordSlot, 2, GL_FLOAT, GL_FALSE, stride, pTexCoords);
        
        glBindTexture(GL_TEXTURE_2D, textures.find(curObj->objectType)->second);
        
        glUniform1i(textureUniform, 0);
        
        glDrawArrays(GL_TRIANGLES, 0, 6);
        
        curObj = curObj->getNextSceneObject();
    }
    
    glDisableVertexAttribArray(positionSlot);
    glDisableVertexAttribArray(colorSlot);
    glDisableVertexAttribArray(texCoordSlot);

    //checkGlError("render scene");
}