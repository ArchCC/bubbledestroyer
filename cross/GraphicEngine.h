//
//  GraphicEngine.h
//  BubbleDestroyer
//
//  Created by Aleksandr Semenov on 15.07.14.
//  Copyright (c) 2014 Aleksandr Semenov. All rights reserved.
//

#ifndef __BubbleDestroyer__GraphicEngine__
#define __BubbleDestroyer__GraphicEngine__

#include <iostream>

#ifdef __APPLE__
    #include <OpenGLES/ES2/gl.h>
    #include <OpenGLES/ES2/glext.h>
#elif __ANDROID__
    #include <GLES2/gl2.h>
    #include <GLES2/gl2ext.h>
#endif

#include <stdlib.h>

#include <map>
#include "SceneObjectQueue.h"

#include "Logger.h"

#define STRINGIFY(A)  #A
#include "SimpleVertex.glsl"
#include "SimpleFragment.glsl"

class GraphicEngine {
public:
    GraphicEngine();
    void Initialize(int width, int height, int xAspect, int yAspect);
    void SetupTexture(int objectType, int frame, void *imgData, GLuint width, GLuint height);
    void RenderScene(SceneObjectQueue*);
private:
    GLuint m_simpleProgram;
    std::map <int, GLuint> textures;
    void ApplyOrtho(float maxX, float maxY) const;
    GLuint BuildShader(const char* source, GLenum shaderType) const;
    GLuint BuildProgram(const char* vertexShaderSource, const char* fragmentShaderSource) const;
    
};

#endif /* defined(__BubbleDestroyer__GraphicEngine__) */
