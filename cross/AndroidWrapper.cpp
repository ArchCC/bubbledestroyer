#include <jni.h>
#include <android/log.h>

#include <stdio.h>
#include <stdlib.h>
#include <math.h>

#include "GameEngine.h"
#include "Logger.h"

extern "C" {
    JNIEXPORT jlong JNICALL Java_ru_cc_bubbledestroyer_androidwrapper_OGLJniLib_createGameEngine(JNIEnv * env, jobject obj,  jfloat width, jfloat height);
    JNIEXPORT void JNICALL Java_ru_cc_bubbledestroyer_androidwrapper_OGLJniLib_setupTexture(JNIEnv * env, jobject obj, jlong nativePointer, jint objType, jint frNumber, jobject buf, jint wid, jint hei);
    JNIEXPORT void JNICALL Java_ru_cc_bubbledestroyer_androidwrapper_OGLJniLib_updateAnimation(JNIEnv * env, jobject obj, jlong nativePointer);
    JNIEXPORT void JNICALL Java_ru_cc_bubbledestroyer_androidwrapper_OGLJniLib_showMainScreen(JNIEnv * env, jobject obj, jlong nativePointer);
    JNIEXPORT void JNICALL Java_ru_cc_bubbledestroyer_androidwrapper_OGLJniLib_startNewDeadlyLine(JNIEnv * env, jobject obj, jlong nativePointer, jfloat x, jfloat y);
    JNIEXPORT void JNICALL Java_ru_cc_bubbledestroyer_androidwrapper_OGLJniLib_addPointToDeadlyLine(JNIEnv * env, jobject obj, jlong nativePointer, jfloat x, jfloat y);
    JNIEXPORT void JNICALL Java_ru_cc_bubbledestroyer_androidwrapper_OGLJniLib_testLine(JNIEnv * env, jobject obj, jlong nativePointer);
};

JNIEXPORT jlong JNICALL Java_ru_cc_bubbledestroyer_androidwrapper_OGLJniLib_createGameEngine(JNIEnv *env, jobject obj, jfloat width, jfloat height) {
	LOGI("create geme engine call\n");
	GameEngine* retGameEngine =  new GameEngine();
	retGameEngine->Initialize(width, height);
    return (jlong)(retGameEngine);
}

JNIEXPORT void JNICALL Java_ru_cc_bubbledestroyer_androidwrapper_OGLJniLib_setupTexture(JNIEnv *env, jobject obj, jlong nativePointer, jint objType, jint frNumber, jobject buf, jint wid, jint hei) {

	char* buffer_address = (char *)env->GetDirectBufferAddress(buf);
	LOGI("jni w:%d,h:%d\n",wid,hei);
    /*if (hei < 10) {
        for (int i = 0; i < wid * hei; i++) {
        	char ch = *(buffer_address+i);
            LOGI("%d",ch);
        }
    }*/

	((GameEngine*)nativePointer)->GetGraphicEngine()->SetupTexture(objType, frNumber, env->GetDirectBufferAddress(buf), wid, hei);
}

JNIEXPORT void JNICALL Java_ru_cc_bubbledestroyer_androidwrapper_OGLJniLib_updateAnimation(JNIEnv *env, jobject obj, jlong nativePointer) {
    ((GameEngine*)nativePointer)->UpdateAnimation();
}

JNIEXPORT void JNICALL Java_ru_cc_bubbledestroyer_androidwrapper_OGLJniLib_showMainScreen(JNIEnv *env, jobject obj, jlong nativePointer) {
    ((GameEngine*)nativePointer)->ShowMainScreen();
}

JNIEXPORT void JNICALL Java_ru_cc_bubbledestroyer_androidwrapper_OGLJniLib_startNewDeadlyLine(JNIEnv * env, jobject obj, jlong nativePointer, jfloat x, jfloat y) {
	((GameEngine*)nativePointer)->startNewDeadlyLine(x, y);;
}

JNIEXPORT void JNICALL Java_ru_cc_bubbledestroyer_androidwrapper_OGLJniLib_addPointToDeadlyLine(JNIEnv * env, jobject obj, jlong nativePointer, jfloat x, jfloat y) {
    ((GameEngine*)nativePointer)->addPointToDeadlyLine(x, y);
}

JNIEXPORT void JNICALL Java_ru_cc_bubbledestroyer_androidwrapper_OGLJniLib_testLine(JNIEnv *env, jobject obj, jlong nativePointer) {
    ((GameEngine*)nativePointer)->testLine();
}