//
//  SceneObject.cpp
//  BubbleDestroyer
//
//  Created by Aleksandr Semenov on 16.07.14.
//  Copyright (c) 2014 Aleksandr Semenov. All rights reserved.
//

#include "SceneObject.h"

SceneObject::SceneObject(int inObjectType)
{
    verteces = GraphicObject::getVertecesForGameObject();
    
    objectType = inObjectType;
    
    rotation[0] = 0;
    rotation[1] = 1;
    
    position[0] = 0;
    position[1] = 0;
    
    scale = 1;
    
    speed[0] = 0;
    speed[1] = 0;
    
    speedAngle[0] = 0;
    speedAngle[1] = 0;
    
    birthTime = std::chrono::high_resolution_clock::now().time_since_epoch() / std::chrono::milliseconds(1);
    
    removed = false;
}

void SceneObject::setNextSceneObject(SceneObject* sceneObject)
{
    nextSceneObject = sceneObject;
}

SceneObject* SceneObject::getNextSceneObject()
{
    return nextSceneObject;
}

void SceneObject::setPrevSceneObject(SceneObject* sceneObject)
{
    prevSceneObject = sceneObject;
}

SceneObject* SceneObject::getPrevSceneObject()
{
    return prevSceneObject;
}

void SceneObject::setParentObject(SceneObject* sceneObject)
{
    parentObject = sceneObject;
}

SceneObject* SceneObject::getParentObject()
{
    return parentObject;
}