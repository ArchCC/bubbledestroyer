//
//  SceneObjectQueue.h
//  BubbleDestroyer
//
//  Очередь для отрисовки объектов сцены
//
//  Created by Aleksandr Semenov on 16.07.14.
//  Copyright (c) 2014 Aleksandr Semenov. All rights reserved.
//

//Вот такая вот коллекция для хранения и более простых манипуляций с очередями для отрисовки, велосипед, но оказалось быстрее чем найти подходящую замену

#ifndef __BubbleDestroyer__SceneObjectQueue__
#define __BubbleDestroyer__SceneObjectQueue__

#include <iostream>
#include "SceneObject.h"

class SceneObjectQueue
{
public:
    void addSceneObject(SceneObject*);
    void removeSceneObject(SceneObject*);
    void replaceWithObject(SceneObject*, SceneObject*);
    SceneObject* getFirstSceneObject();
    void clear();
private:
    SceneObject* firstSceneObject = nullptr;
    SceneObject* lastSceneObject = nullptr;
    void addSceneObjectAfter(SceneObject*, SceneObject*);
};

#endif /* defined(__BubbleDestroyer__SceneObjectQueue__) */
