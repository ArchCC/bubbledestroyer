//
//  BDCLine.cpp
//  BubbleDestroyer
//
//  Created by Aleksandr Semenov on 19.07.14.
//  Copyright (c) 2014 Aleksandr Semenov. All rights reserved.
//

//Вот такая вот ерунда используется для хранения точек линии, которой разрезаются шары

#include "BDCLine.h"

void BDCLine::addPoint(float x, float y) {
    BDCPoint newPoint = *new BDCPoint;
    newPoint.x = x;
    newPoint.y = y;
    points.push_back(newPoint);
}

std::vector<BDCPoint> BDCLine::getPoints() {
    return points;
}

void BDCLine::clear() {
    points.erase(points.begin(),points.end());
}