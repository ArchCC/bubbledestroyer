//
//  GraphicObject.h
//  BubbleDestroyer
//
//  Created by Aleksandr Semenov on 16.07.14.
//  Copyright (c) 2014 Aleksandr Semenov. All rights reserved.
//

//Графическое описание объекта для отрисовки

#ifndef __BubbleDestroyer__GraphicObject__
#define __BubbleDestroyer__GraphicObject__

#include <iostream>
class GraphicObject {
public:
    struct Vertex {
        float Position[2];
        float Color[4];
        float TexCoord[2];
    };
    
    static Vertex* getVertecesForGameObject();
};

//Вершины для всех используемых объектов, т.к. все очень квадратное и однотипное =)
static GraphicObject::Vertex VerticesBubble[] = {
    {{-1, 1}, {1, 1, 1, 0}, {1, 1}},
    {{1, 1},  {1, 1, 1, 0}, {0, 1}},
    {{1, -1}, {1, 1, 1, 0}, {0, 0}},
    {{1, -1}, {1, 1, 1, 0}, {0, 0}},
    {{-1, -1},{1, 1, 1, 0}, {1, 0}},
    {{-1, 1}, {1, 1, 1, 0}, {1, 1}},
};

#endif /* defined(__BubbleDestroyer__GraphicObject__) */
