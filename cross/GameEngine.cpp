//
//  GameEngine.cpp
//  BubbleDestroyer
//
//  Created by Aleksandr Semenov on 15.07.14.
//  Copyright (c) 2014 Aleksandr Semenov. All rights reserved.
//

#include "GameEngine.h"
#include <time.h>

//=====================================================================================================
// Вспомогательные функции
//=====================================================================================================

//Генераторы случайных чиселок
float generateFloat(float Low, float Hi) {
    return Low + static_cast <float> (rand()) /( static_cast <float> (RAND_MAX/(Hi - Low)));
}

int generateInt(int Low, int Hi) {
    return Low + static_cast <int> (rand()) /( static_cast <int> (RAND_MAX/(Hi)));                         //<<<<<<<<<<<<<<<<< bug?
}

//Получение отношения сторон экранов
int gcd (int a, int b) {
    return (b == 0) ? a : gcd (b, a%b);
}

static double now_ms(void) {
#ifdef __APPLE__
    return Time::now().time_since_epoch() / std::chrono::milliseconds(1);
#elif __ANDROID__
    struct timespec res;
    clock_gettime(CLOCK_REALTIME, &res);
    return 1000.0 * res.tv_sec + (double) res.tv_nsec / 1e6;
#endif
}

//Функция для нахождения точек пересечения прямой и окружности
//http://www.melloland.com/scripts-and-tutos/collision-detection-between-circles-and-lines
int lineInCircle(double ax, double ay, double bx, double by, double cx, double cy, double cr)
{
    int intersections = 0;
    
    double vx = bx - ax;
    double vy = by - ay;
    double xdiff = ax - cx;
    double ydiff = ay - cy;
    double a = pow(vx, 2) + pow(vy, 2);
    double b = 2 * ((vx * xdiff) + (vy * ydiff));
    double c = pow(xdiff, 2) + pow(ydiff, 2) - pow(cr, 2);
    double quad = pow(b, 2) - (4 * a * c);
    //std::cout << "--- D: " << quad << "\n";
    if (quad >= 0)
    {
        // An infinite collision is happening, but let's not stop here
        float quadsqrt=sqrt(quad);
        for (int i = -1; i <= 1; i += 2)
        {
            // Returns the two coordinates of the intersection points
            float t = (i * -b + quadsqrt) / (2 * a);
            float x = ax + (i * vx * t);
            float y = ay + (i * vy * t);
            // If one of them is in the boundaries of the segment, it collides
            //if (x >= std::min(ax, bx) && x <= std::max(ax, bx) && y >= std::min(ay, by) && y <= std::max(ay, by)) return true;
            //std::cout << "--- " << (x >= std::min(ax, bx) && x <= std::max(ax, bx) && y >= std::min(ay, by) && y <= std::max(ay, by));
            if (x >= std::min(ax, bx) && x <= std::max(ax, bx) && y >= std::min(ay, by) && y <= std::max(ay, by)) {
                //Увеличиваем счетчик пересечения окружности
                intersections++;
            }
        }
    }
    return intersections;
}

//Ужасный по производительности расчет пересечения, который запускается в отдельном потоке, дабы визутально выглядело ничего
void testingLineRoutine(BDCLine deadlyLine, SceneObjectQueue* sceneObjectQueue) {
    //Перебор по каждому шарику из самописной очереди
    SceneObject* curObj = sceneObjectQueue->getFirstSceneObject();
    for (;;) {
        if (curObj == nullptr) {
            break;
        }
        
        //Количество пересечений
        int intersections = 0;
        //Перебор всех отрезков для нашей deadly line
        for (int i = 0; i < deadlyLine.getPoints().size() - 1; i++) {
            //Находим количество пересечений для данного отрезка
            intersections += lineInCircle(deadlyLine.getPoints()[i].x, deadlyLine.getPoints()[i].y, deadlyLine.getPoints()[i+1].x, deadlyLine.getPoints()[i+1].y, curObj->position[0], curObj->position[1], curObj->scale);
            if (intersections > 1) {
                //Когда пересекли 2 раза уничтожаем шарик
                curObj->removed = true;
                break;
            }
        }
        
        //Переходим к следующему шарику
        curObj = curObj->getNextSceneObject();
    }
}

//Преобразование пиксельных координат к тем, которые используем в openGl
float GameEngine::convertXPoint(int x) {
    return x/xRatio - xAspect;
}

//Преобразование пиксельных координат к тем, которые используем в openGl
float GameEngine::convertYPoint(int y) {
    return -(y/yRatio - yAspect);
}

void GameEngine::startNewDeadlyLine(int x, int y) {
    if (!IsGameStarted()) {
        StartGame();
        return;
    }
    LOGI("New line at %d %d\n", x, y);
    deadlyLine.clear();
    deadlyLine.addPoint(convertXPoint(x), convertYPoint(y));
}

void GameEngine::addPointToDeadlyLine(int x, int y) {
    if (!IsGameStarted()) {
        StartGame();
        return;
    }
    LOGI("Next point at %d %d\n", x, y);
    deadlyLine.addPoint(convertXPoint(x), convertYPoint(y));
}

GraphicEngine* GameEngine::GetGraphicEngine() {
    return graphicEngine;
}

//Отрисовка в буфер openGl
void GameEngine::RenderScene() {
    graphicEngine->RenderScene(sceneObjectQueue);
}

bool GameEngine::IsGameStarted() {
    return gameIsStarted;
}

//=====================================================================================================
// Инициализация
//=====================================================================================================
void GameEngine::Initialize(int width, int height)
{
    LOGI("game engine initialization\n");
    //Вычисляем отношения сторон для экрана
    int ratio = gcd(height, width);
    xAspect = width / ratio;
    yAspect = height / ratio;
    
    //Инициализация графического движка
    graphicEngine->Initialize(width, height, xAspect, yAspect);

    //Используется для преобразования пиксельных координат в openGl
    xRatio = width / (xAspect * 2);
    yRatio = height / (yAspect * 2);

    //Вот такая очередь для отрисовки объектов
    sceneObjectQueue = new SceneObjectQueue();
    
    //Таймер для расчета положения объектов
    t0 = now_ms();
}

//=====================================================================================================
// Игровая логика
//=====================================================================================================

//Проверка шариков на разрезание происходит в отдельном потоке, воизбежание визуальных задержек в движении шариков
void GameEngine::testLine()
{
    if (!IsGameStarted()) {
        StartGame();
        return;
    }
    
    if(deadlyLine.getPoints().size() < 2) {
        return;
    }
    LOGI("Test line\n");
    std::thread(testingLineRoutine, deadlyLine, sceneObjectQueue).detach();
}

//Основной расчет положения объектов и другой игровой логики
void GameEngine::UpdateAnimation()
{
    if (!IsGameStarted()) {
        return;
    }
    
    //Таймер 2 для расчета положения
    t1 = now_ms();
    
    //Проверка на количество уничтоженных темных шаров, при уничтожении 3го останавливаем игру
    if (darkDestroyed > 2) {
        StopGame();
        return;
    }
    
    //Возможно знакомый нам по предыдушим файлам цикл по всем шарам из самописной очереди...
    SceneObject* curObj = sceneObjectQueue->getFirstSceneObject();
    for (;;) {
        if (curObj == nullptr) {
            break;
        }
        
        //При уничтожении шара удаляем его из очереди
        if (curObj->removed) {
            sceneObjectQueue->removeSceneObject(curObj);
            
            //Если это был темный шар то неминуемо движемся к концу игры
            //В теории мы еще успеем обсчитать 1 такт для отрисовки-логики, но что нам жалко что ли...
            if (curObj->objectType == 1) {
                darkDestroyed++;
            }
            
            //Очистка памяти
            SceneObject* objToRemove = curObj;
            curObj = curObj->getNextSceneObject();
            delete objToRemove;
            
            continue;
        }
        
        //Сдвиг положения шарика в зависимости от его скорости и времени с пердыдущего тика
        curObj->position[1] += (t1 - t2) / 1000 * curObj->speed[1];
        
        //Удаление вышедших за экран шаров
        if (curObj->position[1] < -yAspect*2) {
            sceneObjectQueue->removeSceneObject(curObj);
            
            //Очистка памяти
            SceneObject* objToRemove = curObj;
            curObj = curObj->getNextSceneObject();
            delete objToRemove;
            
            continue;
        }
        
        //Следующий шар
        curObj = curObj->getNextSceneObject();
    }
    
    //Раз в секунду сбрасываем ограничение на количество генерируемых шаро
    if ((t1 - t0) > 1000) {
        canBeGenerated = 3;
        t0 = t1;
    }
    
    //Если есть возможность и удача генерируем новый шар, с вероятностью нужно поиграться, а то возникают почти сразу
    if (canBeGenerated > 0 && generateInt(1, 2)%2==1) {
        //Случайный тип светлый/темный
        SceneObject* newBubble = new SceneObject(generateInt(1, 2) % 2);
        //Случайный размер в зависимости от экрана
        newBubble->scale = generateFloat(xAspect/15, xAspect/6);
        //Случайная позиция по x
        newBubble->position[0] = generateFloat(-xAspect, xAspect);
        //Сверху экрана
        newBubble->position[1] = yAspect;
        //Случайная скорость
        newBubble->speed[1] = generateFloat(-yAspect/5, -yAspect/15);
        
        //Добавляем в нашу самоделку
        sceneObjectQueue->addSceneObject(newBubble);
        canBeGenerated--;
    }
    
    t2 = t1;
    
    RenderScene();
}

//Отображение экрана с "меню"
void GameEngine::ShowMainScreen()
{
    LOGI("Show main screen");
    SceneObject *mainScreen = new SceneObject(2);
    mainScreen->scale = xAspect;
    sceneObjectQueue->clear();
    sceneObjectQueue->addSceneObject(mainScreen);
    RenderScene();
}

//Запуск игры
void GameEngine::StartGame()
{
    sceneObjectQueue->clear();
    gameIsStarted = true;
    darkDestroyed = 0;
}

//Остановка игры
void GameEngine::StopGame()
{
    gameIsStarted = false;
    ShowMainScreen();
}