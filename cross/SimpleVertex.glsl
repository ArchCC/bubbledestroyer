static const char* SimpleVertexShader = STRINGIFY(
                                                  
attribute vec4 SourceColor;
varying vec4 DestinationColor;

attribute vec4 Position;
attribute vec2 Translation;
attribute vec2 Rotation;
attribute float Scale;

uniform mat4 Projection;

attribute vec2 TexCoordIn; // texturing
varying vec2 TexCoordOut; // texturing

void main(void)
{
    DestinationColor = SourceColor;
    
    mat4 Modelview = mat4(-Rotation.y * Scale,   -Rotation.x * Scale, 0, 0,
                          Rotation.x * Scale,   -Rotation.y * Scale, 0, 0,
                          0,                     0, 1, 0,
                          Translation.x,         Translation.y, 0, 1);
    gl_Position = Projection * Modelview * Position;
    TexCoordOut = TexCoordIn; // texturing
}
);