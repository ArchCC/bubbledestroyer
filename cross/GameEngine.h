//
//  GameEngine.h
//  BubbleDestroyer
//
//  Created by Aleksandr Semenov on 15.07.14.
//  Copyright (c) 2014 Aleksandr Semenov. All rights reserved.
//

#ifndef __BubbleDestroyer__GameEngine__
#define __BubbleDestroyer__GameEngine__

#include <iostream>
#include <future>
#include <stdlib.h>
#include "GraphicEngine.h"
#include "SceneObjectQueue.h"
#include <math.h>
#include "BDCLine.h"
#include "Logger.h"

class GameEngine {
public:
    GameEngine(){
        graphicEngine = new GraphicEngine();
    }
    void Initialize(int width, int height);
    void ShowMainScreen();
    bool IsGameStarted();
    void StartGame();
    void UpdateAnimation();
    GraphicEngine* GetGraphicEngine();
    void RenderScene();
    void testLine();
    void startNewDeadlyLine(int, int);
    void addPointToDeadlyLine(int, int);
private:
    
    void StopGame();
    bool gameIsStarted = false;
    
    SceneObjectQueue* sceneObjectQueue;
    
    GraphicEngine* graphicEngine;
    
    int canBeGenerated = 3;
    int darkDestroyed = 0;
    
    float xRatio,yRatio;
    float xAspect,yAspect;
    float convertXPoint(int);
    float convertYPoint(int);
    
    BDCLine deadlyLine;
};

typedef std::chrono::high_resolution_clock Time;
typedef std::chrono::milliseconds ms;
typedef std::chrono::duration<float> fsec;
static double t0 = 0;
static double t1 = 0;
static double t2 = 0;

#endif /* defined(__BubbleDestroyer__GameEngine__) */
